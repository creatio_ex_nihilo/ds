package ab0_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {

    @Override
    public void run() {
        // try to create a new server
        try (ServerSocket s = new ServerSocket(Main.PORT)) {
            System.out.println("Server started ...");
            // create thread pool which will be used for all connections
            ExecutorService threadPool = Executors.newFixedThreadPool(10);
            while (true) {
                // a "new" thread tries to handle the new connection
                // accepts is blocking (waits till someone tries to connect)
                threadPool.execute(new ServerStuff(s.accept()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ServerStuff implements Runnable {

        private Socket connectedSocket;

        ServerStuff(Socket connectedSocket) {
            this.connectedSocket = connectedSocket;
        }

        @Override
        public void run() {
            System.out.println("connected to socket " + this.connectedSocket);
            try {
                // reads/writes from/to the connected socket (client)
                BufferedReader in = new BufferedReader(new InputStreamReader(this.connectedSocket.getInputStream()));
                PrintWriter out = new PrintWriter(this.connectedSocket.getOutputStream(), true);
                String input;
                while ((input = in.readLine()) != null) {
                    System.out.println("server: " + input);
                    out.println(this.calcStuff(input));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    // because - I think - the default transport protocol is TCP
                    // you need to close the connection to be able to reallocate
                    // the allocated resources for this socket
                    this.connectedSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private String calcStuff(String msg) {
            String[] params = this.splitStuff(msg);
            try {
                java.lang.reflect.Method method = this.getClass().getDeclaredMethod(params[1], double.class);
                String r = (String) method.invoke(this, Double.valueOf(params[2]));
                return String.format("%s for %s: %s", params[1], params[2], r);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        private String root(double number){
            return String.valueOf(Math.sqrt(number));
        }

        private String perimeter(double number){
            return String.valueOf(2 * Math.PI * number);
        }

        private String primes(double number) {
            Vector<Integer> out = new Vector<>();
            for (int i = 1; i <= number; i++) {
                if (this.checkIfPrime(i)) {
                    out.add(i);
                }
            }
            return out.toString();
        }

        private boolean checkIfPrime(int n) {
            for (int i = 2; i <= n / 2; i++) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        }

        private String[] splitStuff(String msg) {
            return msg.split(String.valueOf(ab1.Main.SEPARATOR));
        }
    }
}
