package ab0_1;

public class PrintThread implements Runnable {

    private int number;

    public PrintThread(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println(String.format("Hello World! I'm Thread %d", number));
        if (number++ < Main.THREAD_CNT) {
            new Thread(new PrintThread(number)).start();
        }
    }
}
