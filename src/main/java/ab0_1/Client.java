package ab0_1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Client extends Thread {

    private String msg;

    public Client(String msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        try (Socket s = new Socket(InetAddress.getLocalHost(), Main.PORT)) {
            System.out.println("connected to the server");
            // reads/writes from/to the connected socket (server)
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            // write the message to the server
            out.println(this.createMsg(s, this.msg));
            String input;
            while ((input = in.readLine()) != null) {
                System.out.println("client: " + input);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createMsg(Socket s, String msg) {
        return s.getInetAddress().getHostAddress() + ":" + s.getLocalPort() + ab1.Main.SEPARATOR + msg;
    }
}
