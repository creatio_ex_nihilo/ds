package ab2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileServer implements Runnable {

    @Override
    public void run() {
        // try to create a new server
        try (ServerSocket s = new ServerSocket(ab2.Main.TCP_PORT)) {
            System.out.println("Server started ...");
            // create thread pool which will be used for all connections
            ExecutorService threadPool = Executors.newFixedThreadPool(10);
            while (true) {
                // a "new" thread tries to handle the new connection
                // accepts is blocking (waits till someone tries to connect)
                threadPool.execute(new ServerStuff(s.accept()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ServerStuff implements Runnable {

        private final String FILES = System.getProperty("user.dir") + "/src/ab2/files/";

        private Socket connectedSocket;
        private DataOutputStream dos;
        private Vector<Byte> toBeSend = new Vector<>();

        ServerStuff(Socket connectedSocket) {
            this.connectedSocket = connectedSocket;
        }

        @Override
        public void run() {
            System.out.println("connected to socket " + this.connectedSocket);
            try {
                // reads/writes from/to the connected socket (client)
                DataInputStream dis = new DataInputStream(new BufferedInputStream(this.connectedSocket.getInputStream()));
                this.dos = new DataOutputStream(new BufferedOutputStream(this.connectedSocket.getOutputStream()));
                String s;
                // the cmd has to be a string, so we can use readUTF()
                do {
                    s = dis.readUTF();
                    this.calcStuff(s);
                    //this.dos.writeInt(toBeSend.size());
                    for (byte b : toBeSend) {
                        this.dos.write(b);
                    }
                    toBeSend.clear();
                    this.dos.flush();
                } while (dis.available() > 0);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    // because - I think - the default transport protocol is TCP
                    // you need to close the connection to be able to reallocate
                    // the allocated resources for this socket
                    this.connectedSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void calcStuff(String msg) {
            String[] params = this.splitStuff(msg);
            try {
                java.lang.reflect.Method method = this.getClass().getDeclaredMethod(params[0], String.class);
                method.invoke(this, params.length == 2 ? params[1] : null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // list is given a param for the purpose of not rewriting calcStuff
        private void list(String s) {
            try {
                File f = new File(FILES);
                if (f.isDirectory()) {
                    for (File tmp : f.listFiles()) {
                        for (byte b : tmp.getName().getBytes()) {
                            toBeSend.add(b);
                        }
                        for (byte b : "\n".getBytes()) {
                            toBeSend.add(b);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void get(String fileName) {
            if (fileName != null) {
                FileInputStream fos;
                try {
                    File f = new File(FILES + fileName);
                    if(f.exists()) {
                        fos = new FileInputStream(f);
                        byte[] fileContent = new byte[(int) f.length()];
                        fos.read(fileContent);
                        for (byte b : fileContent) {
                            toBeSend.add(b);
                        }
                    } else {
                        for(byte b : Main.ERROR.getBytes()){
                            toBeSend.add(b);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private String[] splitStuff(String msg) {
            return msg.split(String.valueOf(Main.SEPARATOR));
        }
    }
}
