package ab2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    final static int MAX_MSG_SIZE = 1000;
    final static int UDP_PORT = 34553;
    final static int TCP_PORT = 33283;
    final static char SEPARATOR = ' ';
    final static String ERROR = "file doesn't exist";

    final static char MULTICAST_SEP = '#';
    final static int MULTICAST_PORT = 34343;
    final static String MULTICAST_ADD = "225.0.0.0";

    public static void main(String[] args) {

        // 2.1
        /*
         * TCP = Transmission Control Protocol
         * stateful aka pretty overhead-heavy (connection build-up and tear-down [3-way handshake])
         * data has an order to it (packets with id [sequence number])
         * has mechanism to deal with problems (msg resent, window resize, etc.)
         * usage: scenarios where time isn't the most important thing, but data integrity is (e.g. file transfer)
         *
         * UDP = User Datagram Protocol
         * stateless aka what's that, "overhead"?
         * data has no specific order
         * doesn't give a damn if there are problems
         * usage: scenarios where time is the most important thing, but data integrity isn't (e.g. streaming, VoIP, etc.)
         */
        /*
        new Thread(new QuoteServer()).start();
        new Thread(new QuoteClient()).start();
        */

        // 2.2
        /*
        new Thread(new FileServer()).start();

        new Thread(new FileClient("list")).start();
        new Thread(new FileClient("get test.txt")).start();
        new Thread(new FileClient("get test_2.txt")).start();
        new Thread(new FileClient("get test.png")).start();
        new Thread(new FileClient("get owl_EDIT.avi")).start();
        */

        // 2.3
        int servers = 7;
        ExecutorService s = Executors.newFixedThreadPool(servers);
        for(int i = 0; i < servers; i++) {
            s.submit(new MulticastServer(i));
        }
        int clients = 4;
        ExecutorService c = Executors.newFixedThreadPool(clients);
        for(int i = 0; i < clients; i++) {
            c.submit(new MulticastClient(i));
            /*
            try {
                // will give the servers enough time
                // to take care of the previous client
                Thread.sleep(330);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */
        }
        c.shutdown();
        s.shutdown();
    }
}
