package ab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Vector;

public class QuoteServer implements Runnable {

    private DatagramSocket connectedSocket;
    private Vector<String> quotes = new Vector<>();

    QuoteServer() {
        try {
            this.connectedSocket = new DatagramSocket(Main.UDP_PORT);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        this.quotes.add("try again ... maybe this time? - me (after running a program unsuccessfully)");
        this.quotes.add("don't quote me on this! - some dude");
        this.quotes.add("the internet was a mistake - Abraham Lincoln");
        this.quotes.add("yays vs. nays ... the nays seem to have it! Oooorrrdeeerrr!! - Speaker of the House");
        this.quotes.add("foo bar blubb test - default 'debug' stuff");
    }

    @Override
    public void run() {
        System.out.println("connected to socket " + this.connectedSocket);
        try {
            byte[] msg = new byte[Main.MAX_MSG_SIZE];
            DatagramPacket input = new DatagramPacket(msg, msg.length);
            this.connectedSocket.receive(input);

            byte[] answer_msg = this.randomQuote().getBytes();
            DatagramPacket output = new DatagramPacket(answer_msg, answer_msg.length, input.getAddress(), input.getPort());
            this.connectedSocket.send(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String randomQuote() {
        return this.quotes.get((int) (Math.random() * (this.quotes.size())));
    }
}
