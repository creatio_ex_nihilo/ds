package ab2;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Vector;

public class FileClient extends Thread {

    private String cmd;

    private static final String FILES = System.getProperty("user.dir") + "/src/ab2/client_files/";

    public FileClient(String msg) {
        this.cmd = msg;
    }

    @Override
    public void run() {
        Vector<Character> readChars;
        Vector<Byte> read;
        try (Socket s = new Socket(InetAddress.getLocalHost(), ab2.Main.TCP_PORT)) {
            System.out.println("connected to the server");
            // reads/writes from/to the connected socket (server)
            DataInputStream dis = new DataInputStream(new BufferedInputStream(s.getInputStream()));
            DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));

            // read keyboard input
            /*
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextLine()) {
                String cmd = scanner.nextLine();
                dos.writeUTF(cmd);
            }
            */

            // write the message to the server
            dos.writeUTF(this.cmd);
            dos.flush();
            byte input;
            readChars = new Vector<>();
            read = new Vector<>();
            do {
                input = dis.readByte();
                readChars.add((char) input);
                read.add(input);
            } while (dis.available() > 0);

            StringBuilder sb = new StringBuilder();
            sb.append("_________________________________\n");
            sb.append(read.toString());
            sb.append("\n\n");
            StringBuilder sb2 = new StringBuilder();
            for (char c : readChars) {
                sb2.append(c);
            }
            sb.append(sb2);
            sb.append("\n_________________________________");
            System.out.println(sb.toString());

            if (!sb2.toString().equals(Main.ERROR)) {
                this.saveFile(read);
            } else {
                System.out.println("Error: " + Main.ERROR);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveFile(Vector<Byte> input) {
        File f = new File(FileClient.FILES + System.nanoTime());
        try {
            FileOutputStream fis = new FileOutputStream(f);
            for (byte b : input) {
                fis.write(b);
            }
            fis.flush();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
