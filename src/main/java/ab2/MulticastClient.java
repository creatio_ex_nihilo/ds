package ab2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

public class MulticastClient implements Runnable {

    static final long WAIT_TIME = 1000;
    private int id;
    private Vector<String> answers = new Vector<>();

    MulticastClient(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        try {
            DatagramSocket s = new DatagramSocket();
            byte[] msg = createMsg().getBytes();
            DatagramPacket p = new DatagramPacket(msg, msg.length, InetAddress.getByName(Main.MULTICAST_ADD), Main.MULTICAST_PORT);
            s.send(p);

            // this will unblock the blocking receive()
            // after WAIT_TIME ms
            new Thread(() -> {
                try {
                    Thread.sleep(WAIT_TIME);
                    s.close();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();

            // will never stop "the usual way"
            // will always throw the exception
            while (!s.isClosed()) {
                msg = new byte[Main.MAX_MSG_SIZE];
                DatagramPacket q = new DatagramPacket(msg, msg.length);
                s.receive(q);
                answers.add(new String(q.getData(), 0, q.getLength()));
            }
        } catch (SocketException e) {
            System.out.println(this.printStuff(answers));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private String createMsg() {
        return "REQ" + Main.MULTICAST_SEP + Main.MULTICAST_PORT + Main.MULTICAST_SEP;
    }

    private long getAverageTime(Vector<String> input){
        long sum = 0;
        for(String s : input){
            sum += Long.valueOf(this.splitTime(s));
        }
        return sum / input.size();
    }

    private String splitTime(String msg){
        return msg.split(Main.MULTICAST_SEP + "")[1];
    }

    private String printStuff(Vector<String> input){
        StringBuilder sb = new StringBuilder();
        sb.append("TC-").append(id).append("\n");
        sb.append(Arrays.toString(input.toArray())).append("\n");
        long tmp = this.getAverageTime(input);
        sb.append(tmp).append("\n");
        sb.append(new Date(tmp).toString()).append("\n");
        sb.append("________________\n");
        return sb.toString();
    }
}
