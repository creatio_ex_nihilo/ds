package ab2;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class QuoteClient extends Thread {

    @Override
    public void run() {
        try (DatagramSocket s = new DatagramSocket()) {
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("connected to the server");
            // empty msg
            byte[] send_msg = "".getBytes();
            // create DatagramPacket
            DatagramPacket send = new DatagramPacket(send_msg, send_msg.length, address, Main.UDP_PORT);
            s.send(send);

            // receive answer from server
            byte[] answer_msg = new byte[Main.MAX_MSG_SIZE];
            DatagramPacket answer = new DatagramPacket(answer_msg, answer_msg.length);
            s.receive(answer);
            System.out.println(new String(answer.getData(), 0, answer.getLength()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

