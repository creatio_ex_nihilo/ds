package ab2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Date;

public class MulticastServer implements Runnable {

    private int id;
    private MulticastSocket s;

    MulticastServer(int id) {
        this.id = id;
        try {
            this.s = new MulticastSocket(Main.MULTICAST_PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            this.s.joinGroup(InetAddress.getByName(Main.MULTICAST_ADD));
            while (true) {
                byte[] msg = new byte[Main.MAX_MSG_SIZE];
                DatagramPacket p = new DatagramPacket(msg, msg.length);
                this.s.receive(p);
                //System.out.println(new String(p.getData(), 0, p.getLength()));

                msg = this.createMsg().getBytes();
                DatagramPacket q = new DatagramPacket(msg, msg.length, p.getAddress(), p.getPort());
                // simulated calc & transmission time
                Thread.sleep((int) (Math.random() * (MulticastClient.WAIT_TIME * 0.6) + 1));
                this.s.send(q);
            }
            // close socket?
            //this.s.leaveGroup(InetAddress.getByName(Main.MULTICAST_ADD));
            //this.s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createMsg() {
        return "TS-" + this.id + Main.MULTICAST_SEP + new Date().getTime() + Main.MULTICAST_SEP;
    }
}
