package ab1;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerTask implements Runnable {

    private ConcurrentLinkedQueue<Integer> inputs;
    private LinkedBlockingQueue<HelperContainer> outputs;

    ProducerTask(ConcurrentLinkedQueue<Integer> inputs, LinkedBlockingQueue<HelperContainer> outputs) {
        this.inputs = inputs;
        this.outputs = outputs;
    }

    @Override
    public void run() {
        while (!this.inputs.isEmpty()) {
            int n = this.inputs.poll();
            this.outputs.add(new HelperContainer(n, Main.getDivisors(n)));
        }
    }
}
