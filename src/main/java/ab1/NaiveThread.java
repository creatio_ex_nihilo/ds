package ab1;

import java.util.concurrent.Callable;

public class NaiveThread implements Callable<HelperContainer> {

    private int start;
    private int end;

    NaiveThread(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public HelperContainer call() throws Exception {
        HelperContainer results = Main.calc(this.start, this.end);
        //System.out.println(String.format("biggest in range %d - %d (%d) is %d", this.start, this.end, results.getCnt(), results.getNumber()));
        return results;
    }
}
