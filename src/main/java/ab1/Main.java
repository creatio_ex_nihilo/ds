package ab1;

import ab0_1.Client;
import ab0_1.Server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static final char SEPARATOR = ',';
    static final int RANGE = 100000;
    static final int THREAD_CNT = 4;

    public static void main(String[] args) {
        // 1.1 without threads
        System.out.println("without threads");
        Main.withoutThreads();
        System.out.println();
        // 1.1 naive threads
        System.out.println(String.format("with %d threads and range-chunks", THREAD_CNT));
        Main.naiveThreads();
        System.out.println();
        // 1.2 thread pool
        System.out.println("with a thread pool and no ranges, just each number separately");
        Main.poolOfThreads();
        System.out.println();
        // 1.3 producer consumer
        System.out.println("threads produce results and save them in a blocking queue");
        Main.producerConsumer();
        System.out.println();
        // 1.4 sockets
        System.out.println("socket calculations");
        Main.socketStuff();
        System.out.println();
    }

    static void socketStuff() {
        new Thread(new Server()).start();
        double d = 53.6;
        new Client(Main.createMsg("primes", d)).start();
        new Client(Main.createMsg("perimeter", d)).start();
        new Client(Main.createMsg("root", d)).start();
    }

    static void producerConsumer() {
        long start = System.nanoTime();

        ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_CNT);
        ConcurrentLinkedQueue<Integer> numbers = new ConcurrentLinkedQueue<>();
        LinkedBlockingQueue<HelperContainer> returnedValues = new LinkedBlockingQueue();
        // fill up the queue with the to-be-calced numbers
        for (int i = 1; i < RANGE; i++) {
            numbers.add(i);
        }

        for (int i = 0; i < THREAD_CNT; i++) {
            threadPool.submit(new ProducerTask(numbers, returnedValues));
        }

        int biggestIndex = 0, biggest = 0;
        HelperContainer tmp;
        for (int i = 1; i < RANGE; i++) {
            try {
                tmp = returnedValues.take();
                if (biggest <= tmp.getCnt()) {
                    biggest = tmp.getCnt();
                    biggestIndex = tmp.getNumber();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        threadPool.shutdown();
        long end = System.nanoTime();
        Main.printStuff(biggest, biggestIndex, end, start);
    }

    static void poolOfThreads() {
        long start = System.nanoTime();

        ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_CNT);
        ConcurrentLinkedQueue<Integer> numbers = new ConcurrentLinkedQueue<>();
        // fill up the queue with the to-be-calced numbers
        for (int i = 1; i < RANGE; i++) {
            numbers.add(i);
        }

        List<Future<HelperContainer>> lists = new ArrayList<>();
        // submit the newly created tasks to the thread pool, which will
        // then do it's work by taking values from the queue
        while (!numbers.isEmpty()) {
            lists.add(threadPool.submit(new PooledTask(numbers.poll())));
        }

        int biggestIndex = 0, biggest = 0;
        HelperContainer tmp;
        for (Future<HelperContainer> f : lists) {
            try {
                tmp = f.get();
                if (biggest <= tmp.getCnt()) {
                    biggest = tmp.getCnt();
                    biggestIndex = tmp.getNumber();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        threadPool.shutdown();
        long end = System.nanoTime();
        Main.printStuff(biggest, biggestIndex, end, start);
    }

    static void naiveThreads() {
        long start = System.nanoTime();

        int chunk = RANGE / THREAD_CNT;
        ArrayList<Future<HelperContainer>> futures = new ArrayList<>();
        for (int i = 0; i < THREAD_CNT; i++) {
            FutureTask<HelperContainer> task = new FutureTask<>(new NaiveThread((i * chunk) + 1, (i + 1) * chunk));
            futures.add(task);
            new Thread(task).start();
        }

        int biggestIndex = 0, biggest = 0;
        HelperContainer tmp;
        for (Future<HelperContainer> f : futures) {
            try {
                tmp = f.get();
                if (biggest <= tmp.getCnt()) {
                    biggest = tmp.getCnt();
                    biggestIndex = tmp.getNumber();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long end = System.nanoTime();
        Main.printStuff(biggest, biggestIndex, end, start);
    }

    static void withoutThreads() {
        long start = System.nanoTime();
        HelperContainer tmp = Main.calc(1, RANGE);
        long end = System.nanoTime();
        Main.printStuff(tmp.getCnt(), tmp.getNumber(), end, start);
    }

    static HelperContainer calc(int start, int end) {
        int tmp, biggestIndex = 0, biggest = 0;
        for (int i = start; i <= end; i++) {
            tmp = Main.getDivisors(i);
            if (biggest <= tmp) {
                biggestIndex = i;
                biggest = tmp;
            }
        }
        return new HelperContainer(biggestIndex, biggest);
    }

    static int getDivisors(int input) {
        HashSet<Integer> divisors = new HashSet<>();
        for (int i = 1; i * i <= input; i++) {
            if (input % i == 0) {
                divisors.add(input / i);
                divisors.add(i);
            }
        }
        return divisors.size();
    }

    static String createMsg(String method, double number) {
        return method + Main.SEPARATOR + number;
    }

    static void printStuff(int v, int i, long end, long start) {
        System.out.println(String.format("the biggest number of divisors (%d) has %d", v, i));
        System.out.println(String.format("the search took %f s", ((end - start) / Math.pow(10, 9))));
    }
}


