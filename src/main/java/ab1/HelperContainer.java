package ab1;

public class HelperContainer {
    private int number;
    private int cnt;

    public HelperContainer(int number, int cnt) {
        this.number = number;
        this.cnt = cnt;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }
}
