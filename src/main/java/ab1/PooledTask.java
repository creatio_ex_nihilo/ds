package ab1;

import java.util.concurrent.Callable;

public class PooledTask implements Callable<HelperContainer> {

    private Integer input;

    PooledTask(Integer input) {
        this.input = input;
    }

    @Override
    public HelperContainer call() throws Exception {
        return new HelperContainer(input, Main.getDivisors(input));
    }
}
