package ab3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Message {

    final static String SEPARATOR = "#";
    private final static String CLIENT_SEP = ";";
    private String sender;
    private Set<String> receivers;
    private String type;
    private StringBuilder msg;

    Message(String sender, Set<String> receivers, String typeOfMessage){
        this.sender = sender.toUpperCase();
        this.receivers = new HashSet<>();
        for(String c : receivers){
            this.receivers.add(c.toUpperCase());
        }
        this.type = typeOfMessage;
        this.msg = new StringBuilder();
    }

    Message(String toBeSplitted){
        String[] tmp = toBeSplitted.split(SEPARATOR);
        this.type = tmp[1];
        this.receivers = new HashSet<>();
        String[] clients = tmp[2].split(CLIENT_SEP);
        if(clients.length > 0 && !Arrays.toString(clients).equals("[]")) {
            for(String c : clients){
                this.receivers.add(c.toUpperCase());
            }
        }
        this.sender = tmp[3].toUpperCase();
        this.msg = new StringBuilder(tmp[4]);
    }

    void append(String msg){
        this.msg.append(msg);
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(SEPARATOR);
        sb.append(this.type);
        sb.append(SEPARATOR);
        for(String i : receivers){
            sb.append(i);
            sb.append(CLIENT_SEP);
        }
        sb.append(SEPARATOR);
        sb.append(this.sender);
        sb.append(SEPARATOR);
        sb.append(this.msg);
        sb.append(SEPARATOR);
        return sb.toString();
    }

    String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    String getSender() {
        return sender;
    }

    public String getMsg() {
        return msg.toString();
    }

    Set<String> getReceivers() {
        return receivers;
    }

    String getReceiversString() {
        StringBuilder sb = new StringBuilder();
        for(String i : receivers){
            sb.append(i).append(", ");
        }
        int lio = sb.toString().lastIndexOf(",");
        return sb.toString().substring(0, lio);
    }
}
