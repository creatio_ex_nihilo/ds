package ab3;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ChatClient implements Runnable {

    private String id;
    private String tmpId;
    private Socket socket;
    private Set<String> possibleClients;
    private String CLIENT_SEPARATOR = "@";

    ChatClient(String id) {
        this.tmpId = id;
        try {
            // connect to the server
            this.socket = new Socket(InetAddress.getLocalHost(), Main.PORT);
            this.socket.setKeepAlive(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("connected to the server");
        try {
            DataInputStream dis = new DataInputStream(new BufferedInputStream(this.socket.getInputStream()));
            DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(this.socket.getOutputStream()));

            // offset
            // send first "hi" msg to server
            Message hi = new Message("", new HashSet<>(), "HI");
            hi.append(this.tmpId);
            dos.writeUTF(hi.toString());
            dos.flush();
            // receive id from server
            hi = new Message(dis.readUTF());
            this.id = hi.getMsg();
            System.out.println(String.format("you are client %s", this.id));

            new Thread(() -> {
                Scanner s = new Scanner(System.in);
                while(true) {
                    // eg. blablabla@1,2
                    Message m = this.createMsgFromInput(s.nextLine());
                    try {
                        // send msgs to server
                        // which will send it to the connected clients
                        dos.writeUTF(m.toString());
                        dos.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            new Thread(() -> {
                while(true) {
                    // receive answer from server
                    try {
                        Message m = new Message(dis.readUTF());
                        switch (m.getType()){
                            case "LIST":
                                this.possibleClients = m.getReceivers();
                                System.out.println("_________________________________");
                                System.out.println("possible clients: " + m.getReceiversString());
                                System.out.println("_________________________________");
                                break;
                            case "MSG_REC":
                                System.out.println(String.format("%s: %s", m.getSender(), m.getMsg()));
                                break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Message createMsgFromInput(String s){
        String[] in = s.split(CLIENT_SEPARATOR);
        String[] r = in[1].split(",");
        HashSet<String> receivers = new HashSet<>();
        for(String tmp : r){
            tmp = tmp.trim().toUpperCase();
            if(this.possibleClients.contains(tmp)) {
                receivers.add(tmp);
            }
        }
        Message out = new Message(this.id, receivers, "MSG_SEND");
        out.append(in[0]);
        return out;
    }
}
