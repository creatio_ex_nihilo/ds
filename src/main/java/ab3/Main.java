package ab3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    static final int PORT = 34343;

    public static void main(String[] args) {
        int servers = 1;
        ExecutorService s = Executors.newFixedThreadPool(servers);
        s.submit(new ChatServer());

        /*
        int clients = 1;
        ExecutorService c = Executors.newFixedThreadPool(clients);
        for (int i = 0; i < clients; i++) {
            c.submit(new ChatClient(i));
        }
        c.shutdown();
        */
        s.shutdown();
    }
}
