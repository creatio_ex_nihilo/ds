package ab3;

import java.util.ArrayList;

public class Packet {

    private ArrayList<Byte> packetContent = new ArrayList<>();

    public void append(String input){
        for(byte b : input.getBytes()){
            this.packetContent.add(b);
        }
    }

    public void append(byte[] input){
        for(byte b : input){
            this.packetContent.add(b);
        }
    }

    public byte[] getPacketContent(){
        byte[] out = new byte[this.packetContent.size()];
        int i = 0;
        for(byte b : this.packetContent){
            out[i++] = b;
        }
        return out;
    }

    public int getPacketSize(){
        return this.packetContent.size();
    }

    public void clear(){
        this.packetContent.clear();
    }

}
