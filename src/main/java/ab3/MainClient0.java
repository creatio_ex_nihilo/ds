package ab3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainClient0 {
    public static void main(String[] args) {
        ExecutorService c = Executors.newFixedThreadPool(10);
        c.submit(new ChatClient("Friend"));
        c.shutdown();
    }
}
