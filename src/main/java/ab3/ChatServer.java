package ab3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer implements Runnable {

    private final String SERVER_ID = "SERVER";
    private ServerSocket socket;
    private HashMap<String, Socket> clients;

    ChatServer() {
        this.clients = new HashMap<>();
    }

    @Override
    public void run() {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        try {
            this.socket = new ServerSocket(Main.PORT);
            while (true) {
                // accept connections from clients
                Socket tmp = this.socket.accept();
                tmp.setKeepAlive(true);
                pool.submit(new ServerStuff(this.clients, tmp));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ServerStuff implements Runnable {

        private Socket client;
        private HashMap<String, Socket> clients;

        ServerStuff(HashMap<String, Socket> clients, Socket client) {
            this.clients = clients;
            this.client = client;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    DataInputStream dis = new DataInputStream(new BufferedInputStream(this.client.getInputStream()));
                    DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(this.client.getOutputStream()));
                    Message m = new Message(dis.readUTF());
                    switch (m.getType()) {
                        case "LIST":
                            // send list
                            this.sendList();
                            break;
                        case "HI":
                            // send client id and add to hashmap
                            String id = m.getMsg().toUpperCase();
                            if (!this.clients.containsKey(id)) {
                                this.clients.put(id, this.client);
                                m = new Message(SERVER_ID, new HashSet<>(), m.getType());
                                m.append(id);
                                dos.writeUTF(m.toString());
                                dos.flush();
                                this.sendList();
                            }
                            break;
                        case "MSG_SEND":
                            if (this.clients.containsKey(m.getSender())) {
                                Socket tmp;
                                m.setType("MSG_REC");
                                for (String c : m.getReceivers()) {
                                    tmp = this.clients.get(c);
                                    this.sendMsg(m, tmp);
                                }
                            }
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void sendMsg(Message m, Socket s) {
            try {
                DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
                Message tmp = new Message(m.getSender(), m.getReceivers(), m.getType());
                tmp.append(m.getMsg());
                dos.writeUTF(tmp.toString());
                dos.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void sendList() {
            Message m = new Message(SERVER_ID, this.clients.keySet(), "LIST");
            m.append("foo");
            for (Socket s : this.clients.values()) {
                this.sendMsg(m, s);
            }
        }
    }
}
