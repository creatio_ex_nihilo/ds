import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PhilosopherMain {

    static final int PORT = 4242;
    static final int OFFSET = 300;

    public static void main(String[] args) {
        try {
            int clients = PhilosopherImpl.FORK_CNT;
            int servers = 1;
            ExecutorService s = Executors.newFixedThreadPool(clients);
            for (int i = 0; i < servers; i++) {
                s.submit(new PhilosopherServer());
            }

            Thread.sleep(OFFSET);

            ExecutorService c = Executors.newFixedThreadPool(clients);
            for (int i = 0; i < clients; i++) {
                c.submit(new PhilosopherClient(i));
            }

            c.shutdown();
            s.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
