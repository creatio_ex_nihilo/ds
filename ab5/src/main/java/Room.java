import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class Room extends Semaphore {

    private int type;
    private int price;
    private HashMap<Integer, Reservation> reservations;

    Room(int type, int price) {
        super(1);
        this.type = type;
        this.price = price;
        this.reservations = new HashMap<>();
    }

    void reserveAt(String name, int arrival, int departure) throws IllegalArgumentException {
        if (arrival < RoomManager.MIN_ARRIVAL || arrival > RoomManager.MAX_DEPARTURE) {
            throw new IllegalArgumentException("arrival outside of range");
        }
        if (departure < RoomManager.MIN_ARRIVAL || departure > RoomManager.MAX_DEPARTURE) {
            throw new IllegalArgumentException("departure outside of range");
        }
        if (arrival > departure) {
            throw new IllegalArgumentException("arrival after departure");
        }

        if (!isReservableAt(arrival, departure)) {
            System.out.println(String.format("not reservable from %d to %d", arrival, departure));
        } else {
            for (int i : this.calcAllDays(arrival, departure)) {
                this.reservations.put(i, new Reservation(this.type, arrival, departure, this.getCalcedPrice(arrival, departure), name));
            }
        }
    }

    boolean isReservableAt(int arrival, int departure) {
        for (int i : calcAllDays(arrival, departure)) {
            if (this.reservations.keySet().contains(i)) {
                return false;
            }
        }
        return true;
    }

    private Vector<Integer> calcAllDays(int arrival, int departure) {
        Vector<Integer> days = new Vector<>();
        for (int i = arrival; i <= departure; i++) {
            days.add(i);
        }
        return days;
    }

    int getCalcedPrice(int a, int d) throws IllegalArgumentException {
        if (a < RoomManager.MIN_ARRIVAL || a > RoomManager.MAX_DEPARTURE) {
            throw new IllegalArgumentException("arrival outside of range");
        }
        if (d < RoomManager.MIN_ARRIVAL || d > RoomManager.MAX_DEPARTURE) {
            throw new IllegalArgumentException("departure outside of range");
        }
        if (a > d) {
            throw new IllegalArgumentException("arrival after departure");
        }
        return ((d - a) + 1) * this.price;
    }

    public int getPrice() {
        return price;
    }

    public int getType() {
        return type;
    }

    String getAllReservations() {
        if (this.reservations.size() != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("one of the rooms type(%d):\n", type));
            for (int i : this.reservations.keySet()) {
                sb.append(String.format("day %d %s \n", i, this.reservations.get(i)));
            }
            return sb.toString();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Room{" +
                "type=" + type +
                ", price=" + price +
                '}';
    }
}
