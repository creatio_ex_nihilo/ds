import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Vector;

public class RoomManagerImpl extends UnicastRemoteObject implements RoomManager {

    private int[] defaultRoomCnt = new int[]{10, 20, 5, 3, 2};
    private int[] defaultRoomPri = new int[]{55, 75, 90, 130, 250};
    private HashMap<Integer, Vector<Room>> rooms;

    RoomManagerImpl() throws RemoteException {
        this.offset();
    }

    @Override
    public String checkAvailabilty(int a, int d) throws RemoteException {
        StringBuilder sb = new StringBuilder();
        sb.append("available rooms: \n________________\n");
        for (Vector<Room> v : rooms.values()) {
            int cnt = 0;
            int price = 0;
            String tmp = null;
            for (Room r : v) {
                try {
                    r.acquire();
                    if (tmp == null) {
                        tmp = r.toString();
                        price = r.getCalcedPrice(a, d);
                    }
                    if (r.isReservableAt(a, d)) {
                        cnt++;
                    }
                    r.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            sb.append(String.format("%2d %s %d\n", cnt, tmp, price));
        }
        return sb.toString();
    }

    @Override
    public String book(String name, int r, int a, int d) throws RemoteException {
        boolean found = false;
        try {
            for (Room room : this.rooms.get(r)) {
                if (!found) {
                    room.acquire();
                    if (room.isReservableAt(a, d)) {
                        room.reserveAt(name, a, d);
                        found = true;
                    }
                    room.release();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (found) {
            return "room reserved";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("room could not be reserved; here are some alternatives:\n\n");
            sb.append(this.checkAvailabilty(a, d));
            return sb.toString();
        }
    }

    @Override
    public String summary() throws RemoteException {
        StringBuilder sb = new StringBuilder();
        String tmp;
        for (Vector<Room> v : rooms.values()) {
            for (Room r : v) {
                tmp = r.getAllReservations();
                if (tmp != null) {
                    sb.append(tmp);
                }
            }
        }
        return sb.toString();
    }

    private void offset() {
        // offset
        this.rooms = new HashMap<>();
        Room r;
        int c, p;
        Vector<Room> rs;
        for (int tmp = 0; tmp < defaultRoomCnt.length; tmp++) {
            c = defaultRoomCnt[tmp];
            p = defaultRoomPri[tmp];
            rs = new Vector<>();
            for (int i = 0; i < c; i++) {
                r = new Room(tmp, p);
                rs.add(r);
            }
            this.rooms.put(tmp, rs);
        }
    }
}
