import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PhilosopherInt extends Remote {
    void think(int philID) throws RemoteException;

    void grabFirstFork(int philID) throws RemoteException;

    void releaseFirstFork(int philID) throws RemoteException;

    void grabSecondFork(int philID) throws RemoteException;

    void releaseSecondFork(int philID) throws RemoteException;

    void eat(int philID) throws RemoteException;
}
