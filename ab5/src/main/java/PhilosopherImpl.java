import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Vector;

public class PhilosopherImpl extends UnicastRemoteObject implements PhilosopherInt {

    static final int FORK_CNT = 5;
    static final int UPPER_TIME_LIMIT = 100;
    private Vector<Fork> forks = new Vector<>();
    private HashMap<Fork, Integer> allocations = new HashMap<>();

    PhilosopherImpl() throws RemoteException {
        super();
        for (int i = 0; i < FORK_CNT; i++) {
            // permits = 1 -> semaphore becomes a mutex
            forks.add(new Fork(1, i));
        }

        new Thread(() -> {
            try {
                // offset
                Thread.sleep(PhilosopherMain.OFFSET + 10);
                while (true) {
                    Thread.sleep((int) (UPPER_TIME_LIMIT * 1.3));
                    // should be done in a synchronized way so nobody modifies the hashmap
                    // concurrently, but meh, it's just to check if allocations are there aka "debug" information
                    System.out.println(":_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:");
                    for (Fork f : this.allocations.keySet()) {
                        System.out.println(String.format("P-%d has fork %d", this.allocations.get(f), f.getId()));
                    }
                    System.out.println(":_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:_:");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static int randomInt() {
        return (int) (Math.random() * (UPPER_TIME_LIMIT - 0) + 1);
        //return UPPER_TIME_LIMIT;
    }

    @Override
    public void think(int philID) throws RemoteException {
        try {
            System.out.println(String.format("P-%d started thinking", philID));
            Thread.sleep(randomInt());
            System.out.println(String.format("P-%d stopped thinking", philID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void grabFirstFork(int philID) throws RemoteException {
        int id = Math.min(philID, (philID + 1) % FORK_CNT);
        this.acquire(philID, id);
    }

    @Override
    public void releaseFirstFork(int philID) throws RemoteException {
        int id = (philID + 1) % FORK_CNT;
        this.release(philID, id);
    }

    @Override
    public void grabSecondFork(int philID) throws RemoteException {
        int id = Math.max(philID, (philID + 1) % FORK_CNT);
        this.acquire(philID, id);
    }

    @Override
    public void releaseSecondFork(int philID) throws RemoteException {
        // redundant, but helps maintain- and readability
        int id = philID;
        this.release(philID, id);
    }

    @Override
    public void eat(int philID) throws RemoteException {
        try {
            // as soon as you check if the allocations are there, they could already be different ...
            /*
            if(cnt(philID) < 2) {
                System.out.println(cnt(philID));
                throw new IllegalStateException("philosopher needs two forks to eat");
            }
             */
            System.out.println(String.format("P-%d started eating", philID));
            Thread.sleep(randomInt());
            System.out.println(String.format("P-%d stopped eating", philID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void acquire(int philID, int id) {
        Fork tmp = this.forks.get(id);
        try {
            tmp.acquire();
            this.put(tmp, philID);
            System.out.println(String.format("P-%d acquire fork %d", philID, id));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void release(int philID, int id) {
        Fork tmp = this.forks.get(id);
        tmp.release();
        this.remove(tmp);
        System.out.println(String.format("P-%d released fork %d", philID, id));
    }

    private synchronized int cnt(int philID) {
        int cnt = 0;
        for (int i : this.allocations.values()) {
            if (i == philID) {
                cnt++;
            }
        }
        return cnt;
    }

    private synchronized void put(Fork tmp, int philID) {
        this.allocations.put(tmp, philID);
    }

    private synchronized void remove(Fork tmp) {
        this.allocations.remove(tmp);
    }
}
