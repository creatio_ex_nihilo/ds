import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class HotelClientCLI {
    public static void main(String[] args) {
        try {
            Registry r = LocateRegistry.getRegistry(RoomManager.PORT);
            RoomManager stub = (RoomManager) r.lookup("rmi://localhost/" + RoomManager.RMI_LOCATION);
            Scanner s = new Scanner(System.in);
            String name = "TEST_" + (int) ((Math.random() + 100) * 899);
            while (true) {
                String[] in = s.nextLine().split(" ");
                switch (in[0]) {
                    case "c":
                        System.out.println(stub.checkAvailabilty(Integer.parseInt(in[1]), Integer.parseInt(in[2])));
                        break;
                    case "b":
                        System.out.println(stub.book(name, Integer.parseInt(in[1]), Integer.parseInt(in[2]), Integer.parseInt(in[3])));
                        break;
                    case "s":
                        System.out.println(stub.summary());
                        break;
                    default:
                        System.out.println("cmd not recognized");
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
