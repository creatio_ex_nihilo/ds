import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class PhilosopherClient implements Runnable {

    private int philID;

    PhilosopherClient(int philID) {
        this.philID = philID;
    }

    @Override
    public void run() {
        try {
            Registry r = LocateRegistry.getRegistry(PhilosopherMain.PORT);
            PhilosopherInt stub = (PhilosopherInt) r.lookup("rmi://localhost/PHILOSOPHER");
            while (true) {
                stub.think(this.philID);
                stub.grabFirstFork(this.philID);
                stub.grabSecondFork(this.philID);
                stub.eat(this.philID);
                stub.releaseFirstFork(this.philID);
                stub.releaseSecondFork(this.philID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
