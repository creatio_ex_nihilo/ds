import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RoomManager extends Remote {
    int PORT = 32322;
    String RMI_LOCATION = "HOTEL";
    int MIN_ARRIVAL = 1;
    int MAX_DEPARTURE = 100;

    String checkAvailabilty(int a, int d) throws RemoteException;

    String book(String name, int r, int a, int d) throws RemoteException;

    String summary() throws RemoteException;
}
