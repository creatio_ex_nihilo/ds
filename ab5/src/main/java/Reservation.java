public class Reservation {
    private int type;
    private int arrival;
    private int departure;
    private int price;
    private String name;

    public Reservation() {
    }

    Reservation(int type, int arrival, int departure, int price, String name) {
        this.type = type;
        this.arrival = arrival;
        this.departure = departure;
        this.price = price;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getArrival() {
        return arrival;
    }

    public void setArrival(int arrival) {
        this.arrival = arrival;
    }

    public int getDeparture() {
        return departure;
    }

    public void setDeparture(int departure) {
        this.departure = departure;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "type=" + type +
                ", arrival=" + arrival +
                ", departure=" + departure +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
