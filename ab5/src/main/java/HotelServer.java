import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class HotelServer {
    public static void main(String[] args) {
        try {
            RoomManagerImpl p = new RoomManagerImpl();
            Registry r = LocateRegistry.createRegistry(RoomManager.PORT);
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(p, true);
            RoomManager stub = (RoomManager) UnicastRemoteObject.exportObject(p, 0);
            r.rebind("rmi://localhost/" + RoomManager.RMI_LOCATION, stub);
            System.out.println("HotelServer running");
        } catch (Exception e) {
            System.out.println("HotelServer not running");
            e.printStackTrace();
        }
    }
}
