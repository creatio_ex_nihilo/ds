import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HotelClient {
    public static void main(String[] args) {
        try {
            Registry r = LocateRegistry.getRegistry(RoomManager.PORT);
            RoomManager stub = (RoomManager) r.lookup("rmi://localhost/" + RoomManager.RMI_LOCATION);
            CommandLine cl = new DefaultParser().parse(initOptions(), args);
            String cmd = cl.getArgs()[0];
            String name = "TEST_" + (int) ((Math.random() + 100) * 899);
            switch (cmd) {
                case "checkAvailability":
                    System.out.println(stub.checkAvailabilty(Integer.parseInt(cl.getArgs()[1]), Integer.parseInt(cl.getArgs()[2])));
                    break;
                case "book":
                    System.out.println(stub.book(name, Integer.parseInt(cl.getArgs()[1]), Integer.parseInt(cl.getArgs()[2]), Integer.parseInt(cl.getArgs()[3])));
                    break;
                case "summary":
                    System.out.println(stub.summary());
                    break;
                default:
                    System.out.println("cmd not recognized");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Options initOptions() {
        Options o = new Options();
        Option a = Option.builder()
                .longOpt("c")
                .hasArgs()
                .numberOfArgs(2)
                .argName("checkAvailability")
                .valueSeparator(' ')
                .build();
        o.addOption(a);
        Option b = Option.builder()
                .longOpt("b")
                .hasArgs()
                .numberOfArgs(3)
                .argName("book")
                .valueSeparator(' ')
                .build();
        o.addOption(b);
        Option s = Option.builder()
                .longOpt("s")
                .hasArg(false)
                .argName("summary")
                .valueSeparator(' ')
                .build();
        o.addOption(s);
        return o;
    }
}
