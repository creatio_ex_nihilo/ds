import java.util.concurrent.Semaphore;

public class Fork extends Semaphore {
    // dead code for first idea to
    // implement Chandy/Misra
    private boolean clean;
    private int id;

    Fork(int permits, int id) {
        super(permits);
        this.clean = false;
        this.id = id;
    }

    int getId() {
        return id;
    }

    public void use() {
        this.clean = false;
    }

    public void clean() {
        this.clean = true;
    }

    public boolean isClean() {
        return clean;
    }
}
