import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class PhilosopherServer implements Runnable {

    @Override
    public void run() {
        try {
            PhilosopherImpl p = new PhilosopherImpl();
            Registry r = LocateRegistry.createRegistry(PhilosopherMain.PORT);
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(p, true);
            PhilosopherInt stub = (PhilosopherInt) UnicastRemoteObject.exportObject(p, 0);
            r.rebind("rmi://localhost/PHILOSOPHER", stub);
            System.out.println("PhilosopherServer running");
        } catch (Exception e) {
            System.out.println("PhilosopherServer not running");
            e.printStackTrace();
        }
    }
}
