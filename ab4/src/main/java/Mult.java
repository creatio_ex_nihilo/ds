import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Mult extends UnicastRemoteObject implements MultInt {
    public Mult() throws RemoteException {
    }

    @Override
    public int mult(int a, int b) throws RemoteException {
        return a * b;
    }
}
