import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StringOpsClient2 {
    public static void main(String[] args) {
        try {
            Registry r = LocateRegistry.getRegistry();
            StringOpsInt stub = (StringOpsInt) r.lookup("rmi://localhost/STRING");
            File f = new File("/home/creatio_ex_nihilo/IdeaProjects/ds/ab4/src/main/misc/text.txt");
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            long start = System.nanoTime();
            while ((line = br.readLine()) != null) {
                System.out.println(stub.uniqueReverse(line));
                Thread.sleep((int) (Math.random() * StringOps.TIME) + 1);
            }
            System.out.println(String.format("time passed: %fs", ((System.nanoTime() - start) / Math.pow(10, 9))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}