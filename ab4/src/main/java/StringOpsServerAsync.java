import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class StringOpsServerAsync {
    public static void main(String[] args) {
        try {
            StringOpsAsyncInt s = new StringOpsAsync();
            Registry r = LocateRegistry.getRegistry();
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(s, true);
            StringOpsAsyncInt stub = (StringOpsAsyncInt) UnicastRemoteObject.exportObject(s, 0);
            r.rebind("rmi://localhost/STRING_ASYNC", stub);
            System.out.println("MultServer running");
        } catch (Exception e) {
            System.out.println("MultServer not running");
            e.printStackTrace();
        }
    }
}
