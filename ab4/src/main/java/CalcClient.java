import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DecimalFormat;
import java.util.Arrays;

public class CalcClient {

    private static final int UPPER_LIMIT = 42;

    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("+#0000.00;-#");
        try {
            Registry r = LocateRegistry.getRegistry();
            CalcInt stub = (CalcInt) r.lookup("rmi://localhost/CALC");
            System.out.println(Arrays.toString(r.list()));
            int t = UPPER_LIMIT;
            while (t-- != 0) {
                double tmp1 = CalcClient.getValue();
                double tmp2 = CalcClient.getValue();
                System.out.println(String.format("%s + %s = %s", df.format(tmp1), df.format(tmp2), df.format(stub.add(tmp1, tmp2))));
            }
            t = UPPER_LIMIT;
            while (t-- != 0) {
                double tmp1 = CalcClient.getValue();
                double tmp2 = CalcClient.getValue();
                System.out.println(String.format("%s - %s = %s", df.format(tmp1), df.format(tmp2), df.format(stub.sub(tmp1, tmp2))));
            }
            t = UPPER_LIMIT;
            while (t-- != 0) {
                double tmp1 = CalcClient.getValue();
                double tmp2 = CalcClient.getValue();
                System.out.println(String.format("%s * %s = %s", df.format(tmp1), df.format(tmp2), df.format(stub.mul(tmp1, tmp2))));
            }
            t = UPPER_LIMIT;
            while (t-- != 0) {
                double tmp1 = CalcClient.getValue();
                double tmp2 = CalcClient.getValue();
                System.out.println(String.format("%s / %s = %s", df.format(tmp1), df.format(tmp2), df.format(stub.div(tmp1, tmp2))));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static double getValue() {
        return (Math.random() * (UPPER_LIMIT - 0) + 1);
    }
}