
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.CompletableFuture;

public class StringOpsClientAsync {
    public static void main(String[] args) {
        try {
            Registry r = LocateRegistry.getRegistry();
            StringOpsAsyncInt stub = (StringOpsAsyncInt) r.lookup("rmi://localhost/STRING_ASYNC");
            File f = new File("/home/creatio_ex_nihilo/IdeaProjects/ds/ab4/src/main/misc/text.txt");
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            long start = System.nanoTime();
            try {
                while ((line = br.readLine()) != null) {
                    StringOpsClientAsync.stuff(stub, line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(String.format("time passed: %fs", ((System.nanoTime() - start) / Math.pow(10, 9))));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void stuff(StringOpsAsyncInt stub, String s) {
        CompletableFuture.supplyAsync(() -> {
            try {
                return stub.uniqueReverse(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "ERROR";
        }).thenApply((in) -> {
            System.out.println(in);
            return null;
        });
    }
}