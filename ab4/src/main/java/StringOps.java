import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class StringOps extends UnicastRemoteObject implements StringOpsInt {

    static double TIME = 2.5;
    private static final String SPLIT = " ";
    private static int CNT = 1;

    StringOps() throws RemoteException {
    }

    @Override
    public String uniqueReverse(String input) throws RemoteException {
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.format("%06d:%s", CNT++, this.uniqueReverseHelper(input));
    }

    private String uniqueReverseHelper(String input) {
        String[] words = input.split(SPLIT);
        StringBuilder sb = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            sb.append(words[i]).append(SPLIT);
        }
        return sb.toString().trim();
    }
}
