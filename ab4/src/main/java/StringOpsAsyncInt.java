import java.rmi.Remote;
import java.rmi.RemoteException;

public interface StringOpsAsyncInt extends Remote {
    String uniqueReverse(String input) throws RemoteException;
}
