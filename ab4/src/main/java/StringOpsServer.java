import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class StringOpsServer {
    public static void main(String[] args) {
        try {
            StringOps s = new StringOps();
            Registry r = LocateRegistry.getRegistry();
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(s, true);
            StringOpsInt stub = (StringOpsInt) UnicastRemoteObject.exportObject(s, 0);
            r.rebind("rmi://localhost/STRING", stub);
            System.out.println("StringOpsServer running");
        } catch (Exception e) {
            System.out.println("StringOpsServer not running");
            e.printStackTrace();
        }
    }
}
