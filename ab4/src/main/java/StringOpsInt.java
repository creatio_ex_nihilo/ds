import java.rmi.Remote;
import java.rmi.RemoteException;

public interface StringOpsInt extends Remote {
    String uniqueReverse(String input) throws RemoteException;
}
