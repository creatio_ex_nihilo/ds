import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalcInt extends Remote {
    double add(double... inputs) throws RemoteException;

    double sub(double... inputs) throws RemoteException;

    double mul(double... inputs) throws RemoteException;

    double div(double... inputs) throws RemoteException;
}
