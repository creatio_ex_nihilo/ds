import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MultInt extends Remote {
    int mult(int a, int b) throws RemoteException;
}
