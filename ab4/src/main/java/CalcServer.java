import java.lang.reflect.Method;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Vector;

public class CalcServer {
    public static void main(String[] args) {
        try {
            Calc c = new Calc();
            Registry r = LocateRegistry.getRegistry();
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(c, true);
            CalcInt stub = (CalcInt) UnicastRemoteObject.exportObject(c, 0);
            r.rebind("rmi://localhost/CALC", stub);
            System.out.println("CalcServer running");
        } catch (Exception e) {
            System.out.println("CalcServer not running");
            e.printStackTrace();
        }
    }

    private static String list(CalcInt stub) {
        Vector<String> excluded = new Vector<>(Arrays.asList("equals", "toString", "hashCode"));
        Vector<String> others = new Vector<>();
        String tmp;
        for (Method m : stub.getClass().getDeclaredMethods()) {
            tmp = m.getName();
            if (!excluded.contains(tmp)) {
                others.add(tmp);
            }
        }
        return others.toString();
    }
}
