import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class StringOpsAsync extends UnicastRemoteObject implements StringOpsAsyncInt {

    private static final String SPLIT = " ";
    static double TIME = 2.5;
    private static int CNT = 1;

    StringOpsAsync() throws RemoteException {
    }

    @Override
    public String uniqueReverse(String input) throws RemoteException {
        return String.format("%06d:%s", inc(), this.uniqueReverseHelper(input));
    }

    private String uniqueReverseHelper(String input) {
        String[] words = input.split(SPLIT);
        StringBuilder sb = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            sb.append(words[i]).append(SPLIT);
        }
        return sb.toString().trim();
    }

    private synchronized int inc() {
        return CNT++;
    }
}
