import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MultServer {
    // start rmiregistry in directory with the stubs!!
    public static void main(String[] args) {
        try {
            Mult m = new Mult();
            Registry r = LocateRegistry.getRegistry();
            // avoids "ExportException: object already exported"
            UnicastRemoteObject.unexportObject(m, true);
            MultInt stub = (MultInt) UnicastRemoteObject.exportObject(m,0);
            r.rebind("rmi://localhost/MULT", stub);
            System.out.println("MultServer running");
        } catch (Exception e) {
            System.out.println("MultServer not running");
            e.printStackTrace();
        }
    }
}
