import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MultClient {

    static final int UPPER_LIMIT = 42;

    public static void main(String[] args) {
        MultInt m;
        try {
            Registry r = LocateRegistry.getRegistry();
            MultInt stub = (MultInt) r.lookup("rmi://localhost/MULT");
            int t = UPPER_LIMIT;
            while (t-- != 0) {
                int tmp1 = MultClient.getValue(UPPER_LIMIT);
                int tmp2 = MultClient.getValue(UPPER_LIMIT);
                System.out.println(String.format("%03d * %03d = %04d", tmp1, tmp2, stub.mult(tmp1, tmp2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int getValue(int upperLimit) {
        return (int) (Math.random() * (upperLimit - 0) + 1);
    }
}