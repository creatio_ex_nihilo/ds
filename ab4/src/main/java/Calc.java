import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Calc extends UnicastRemoteObject implements CalcInt {
    Calc() throws RemoteException {
    }

    private double calc(String operator, double... input) {
        if (input.length < 2) {
            throw new IllegalArgumentException("insufficient args count");
        }
        try {
            double out = input[0];
            java.lang.reflect.Method method = this.getClass().getDeclaredMethod(operator, double.class, double.class);
            for (int i = 1; i < input.length; i++) {
                out = (double) method.invoke(this, out, input[i]);
            }
            return out;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public double add(double... inputs) throws RemoteException {
        return this.calc("add", inputs);
    }

    private double add(double a, double b) {
        return a + b;
    }

    @Override
    public double sub(double... inputs) throws RemoteException {
        return this.calc("sub", inputs);
    }

    private double sub(double a, double b) {
        return a - b;
    }

    @Override
    public double mul(double... inputs) throws RemoteException {
        return this.calc("mul", inputs);
    }

    private double mul(double a, double b) {
        return a * b;
    }

    @Override
    public double div(double... inputs) throws RemoteException {
        return this.calc("div", inputs);
    }

    private double div(double a, double b) {
        return a / b;
    }
}
