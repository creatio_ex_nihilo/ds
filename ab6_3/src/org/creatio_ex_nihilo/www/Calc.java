package org.creatio_ex_nihilo.www;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

// The Java class will be hosted at the URI path "/calc"
@Path("/calc")
public class Calc {
    // The Java method will process HTTP GET requests

    @GET
    @Path("/")
    public Response greet() {
        String out = "<h1>Welcome to GrossNetCalc!</h1><br/>" +
                "<form  action='/RESTfulWebService_war_exploded/calc/result' method='post'>" +
                "<p>Income: <input name='income' type='number' step='0.01'></p>" +
                "<p>Tax: <input name='tax' type='number' step='0.01'></p>" +
                "<p>Calculate: <select name='drop'><option value='gross'>Gross</option><option value='net'>Net</option></select></p>" +
                "<input type='submit' value='Submit'>" +
                "</form>";
        return Response.status(200).entity(out).build();
    }

    @POST
    @Path("/result")
    public Response getForm(
            @FormParam("income") String income,
            @FormParam("tax") String tax,
            @FormParam("drop") String temp
    ) throws Exception {
        String out;

        if (temp.equals("gross")) {
            out = getGross(Double.parseDouble(income), Double.parseDouble(tax));
        } else {
            out = getNet(Double.parseDouble(income), Double.parseDouble(tax));
        }

        return Response.status(200).entity(out).build();
    }

    public String getGross(double income, double tax) {
        String out = "<b>Income:</b> " + income + "<br/><b>Tax:</b> " + tax + "<br/><b>Gross:</b> " + income * (1 + tax);
        return out;
    }

    public String getNet(double income, double tax) {
        String out = "<b>Income:</b> " + income + "<br/><b>Tax:</b> " + tax + "<br/><b>Net:</b> " + income / (1 + tax);
        return out;
    }


}